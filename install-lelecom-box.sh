#! /bin/bash

# bash <(curl -fsSL https://goo.gl/rjAaVX)

#-----------
# Bootstrap
#-----------

sudo apt-get update -y
sudo apt-get install git python3-pip -y
sudo pip3 install ansible

cd ~pi 
git clone https://gitlab.com/hydrocontest/get-telecom-box.git ansible

cd ansible
sudo ansible-playbook site.yml
