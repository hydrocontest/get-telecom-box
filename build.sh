#!/bin/bash

BUILD=0
while getopts r opt; do
    case "${opt}" in
    r) BUILD=1;;
    esac
done

function copy_python_dist {
    if [[ $BUILD -eq 1 ]]; then
        pushd ../$1
        bash build.sh
        popd
    fi
    rm roles/$2/files/*.whl
    rm roles/$2/files/*.service
    cp ../$1/dist/*.whl roles/$2/files/
    cp ../$1/*.service roles/$2/files/
}

copy_python_dist boat-gateway boatlink-gateway
copy_python_dist data-logger boatlink-logger
copy_python_dist broadcaster boatlink-broadcaster
copy_python_dist box-health boatlink-box-health

pushd ../webapp
    if [[ $BUILD -eq 1 ]]; then
        bash build.sh
    fi
popd
rm roles/boatlink-web/files/*.tar.gz
cp ../webapp/*.tar.gz roles/boatlink-web/files/
